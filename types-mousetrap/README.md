# Installation
> `npm install --save @types/mousetrap`

# Summary
This package contains type definitions for Mousetrap (http://craig.is/killing/mice).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/mousetrap.

### Additional Details
 * Last updated: Mon, 26 Apr 2021 21:31:24 GMT
 * Dependencies: none
 * Global values: `Mousetrap`

# Credits
These definitions were written by [Dániel Tar](https://github.com/qcz), [Alan Choi](https://github.com/alanhchoi), [Nic Barker](https://github.com/nicbarker), [Mitsuka Hanakura a.k.a ragg](https://github.com/ra-gg), and [Piotr Błażejewicz](https://github.com/peterblazejewicz).
